<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'CallIndexController@index');

Route::get('/allformrs', 'TbformsController@allTbForms');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('OPD','OPDController');

