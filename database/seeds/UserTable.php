<?php

use Illuminate\Database\Seeder;

class UserTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        UsersTable::create([
            'name'      =>  'ali',
            'email'     =>  'admin@admin.com',
            'password'  =>  bcrypt('password'),
        ]);

    }
}
