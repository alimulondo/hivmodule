<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class smcfollowupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\smcfollowup::class,10)->create();
        // DB::table('smcfollowups')->insert([
        //     'sitetype' => str_random(10),
        //     'CTL' => str_random(10),
        //     'NatID' => str_random(10),
        //     'SerID' => str_random(10),
        //     'Date' => Carbon::now(),
        //     'wound' => str_random(10),
        //     'missedapt' => str_random(10),
        //     'visittype' => str_random(10),
        //     'AE' => str_random(10),
        //     'severity' => str_random(10),
        //     'tmtgiven' => str_random(10),
        // ]);

    }
}
