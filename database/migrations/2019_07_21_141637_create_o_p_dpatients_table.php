<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOPDpatientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('o_p_dpatients', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('visit_date');
            $table->string('serial_number');
            $table->string('muac_cm');
            $table->string('mauc');
            $table->string('weight');
            $table->string('height');
            $table->string('bmi');
            $table->string('age_of_weight_zscore');
            $table->string('height_of_age_zscore');
            $table->string('blood_pressure_systolic');
            $table->string('blood_pressure_diastolic');
            $table->string('blood_sugar');
            $table->integer('temp');
            $table->string('next_of_kin');
            $table->string('palliative_care');
            $table->string('patient_classification');
            $table->string('tobacco');
            $table->string('alcohol');
            $table->string('fever');
            $table->string('test_done');
            $table->string('results');
            $table->string('results_of_new_presumed_case')->nullable;
            $table->string('sent_to_lab')->nullable;
            $table->string('lab_test_results');
            $table->string('linked_to_TB');
            $table->string('diagnosis')->nullable;
            $table->string('drug')->nullable;
            $table->string('units_per_day')->nullable;
            $table->string('doses_per_day');
            $table->string('time1')->nullable;
            $table->string('time2');
            $table->string('disability');
            $table->string('type_of_disability');
            $table->string('device_provided');
            $table->string('refferal_in_number');
            $table->string('refferal_out_number');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('o_p_dpatients');
    }
}
